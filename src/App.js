import React, { Component } from 'react';
import './App.css';
import MainContent from './components/main-content/main-content';
import VerticalNav from './components/vertical-nav/vertical-nav';
import Projects from './components/projects/projects';
import MyComponent404 from './components/My404Component/My404Component';
import { Route, Redirect } from 'react-router-dom';

class App extends Component {
    render(routing) {
      return(
        <div className="App">
        <VerticalNav/>
        <hr className="vertical-hr"></hr>
          <Redirect from="/" to="/about"/>
          <Route exact path="/about" component={MainContent}/>
          <Route path="/projects" component={Projects}/>
        </div>
      );
    }
}
export default App;