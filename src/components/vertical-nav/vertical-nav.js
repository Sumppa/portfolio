import React, { Component } from 'react';
import './vertical-nav.css';
import ProfileImage from './../../img/me.png';
import GitlabIcon from './../../img/gitlab.png';
import LinkedInIcon from './../../img/linkedin-75px.png';
import { Link } from 'react-router-dom';

class VerticalNav extends Component {
    render() {
        return (
            <div className="VerticalNav">
                <img className="self-image" src={ProfileImage} alt="profile"></img>
                <h1>Taneli Sormunen</h1>
                <hr></hr>
                    <Link className="Link" to="/about">About</Link>
                    <Link className="Link" to="/projects">Projects</Link>
                <br></br>
                <div className="media-icon-wrapper">
                <p>Email:</p>
                <p>taneli.sormunen(at)gmail.com</p>
                    <a href="https://gitlab.com/Sumppa" target="_blank"rel="noopener noreferrer"><img className="media-icon" src={GitlabIcon} alt="gitlab"></img></a>
                    <a href="https://www.linkedin.com/in/taneli-sormunen/" rel="noopener noreferrer" target="_blank"><img className="media-icon" src={LinkedInIcon} alt="linkedin"></img></a>
                </div>
            </div>
        );
    }
}
export default VerticalNav;