import React, { Component } from 'react'
import './My404Component.css';

class My404Component extends Component {
    render() {
        return (
            <div className="My404Component">
                <h1>404 Not Found</h1>
            </div>
        )
    }
}
export default My404Component