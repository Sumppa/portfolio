import React, { Component } from 'react'
import './projects.css';

class Projects extends Component {
    render() {
        return (
            <div className="Projects">
                <h1>Projects</h1>
                <p>Here i will list some of my current and past projects, most of the source codes can be viewed <a href="https://gitlab.com/Sumppa" rel="noopener noreferrer" target="_blank">HERE in my gitlab</a>.</p>
                <div className="list-wrapper">
                    <ul className="nb">
                        <h3>Current projects:</h3>
                        <li><h3>Discord bot</h3>Currently working on a <span className="hlspan">Discord bot</span> that fetches data from <span className="hlspan">Twitch API</span>. Currently you can check if streamer is online or offline with chat command in discord.</li>
                        <li><h3>Portfolio</h3>Im also working on this <span className="hlspan">portfolio</span>, adding some new content, features and improving the functionality.</li>
                    </ul>
                    <ul className="nb">
                        <h3>Past projects:</h3>
                        <li>
                            <h3>WimmaLab</h3>
                            <p>During summer 2018 I worked at WimmaLab as a Junior Web Developer. Our main goal was to create informational and fresh looking website for WimmaLab. I worked mostly with the technical stuff (React, NodeJs).</p>
                            <a href="http://www.wimmalab.org" target="_blank" rel="noopener noreferrer">WimmaLab.org</a>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}
export default Projects