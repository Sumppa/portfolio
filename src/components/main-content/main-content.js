import React, { Component } from 'react';
import './main-content.css';

class MainContent extends Component {
    render() {
        return (
            <div className="MainContent">
                <h1>About</h1>
                <p>Hello there and welcome to my portfolio, my name is Taneli Sormunen and I am 23 years old media engineering student at JAMK university of applied sciences. On this page you will find more about my skills, projects and hobbies.
                </p>
                <div className="list-wrapper">
                    <ul className="nb">
                        <h3>Skills:</h3>
                        <li>JavaScript</li>
                        <li>NodeJs</li>
                        <li>React</li>
                        <li>Mysql</li>
                        <li>C#</li>
                        <li>C++</li>
                        <li>Php / Laravel</li>
                        <li>Android SDK</li>
                        <li>Blender</li>
                    </ul>
                    <ul className="nb">
                        <h3>Things I like:</h3>
                        <li>Coffee / Green tea</li>
                        <li>Twitch.tv</li>
                        <li>Video games / Board games</li>
                        <li>Dungeons & Dragons</li>
                        <li>Japanese culture</li>
                        <li>Nature</li>
                        <li>Linux</li>
                    </ul>
                </div>
            </div>
        );
    }
}
export default MainContent;